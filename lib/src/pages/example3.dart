
import 'package:flutter/material.dart';

class Example3 extends StatelessWidget {
  Example3({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listview3'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/1.jpg'),
            ),
            title: Text(
              'Ice cream',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'ไอศกรีม',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.icecream_sharp,
              size: 25,
            ),
            onTap: () {
              print('Ice cream');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/2.jpg'),
            ),
            title: Text(
              'Cake',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'เค้ก',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.cake,
              size: 25,
            ),
            onTap: () {
              print('Cake');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/3.jpg'),
            ),
            title: Text(
              'Bread',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'ขนมปัง',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.breakfast_dining,
              size: 25,
            ),
            onTap: () {
              print('Bread');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/4.jpg'),
            ),
            title: Text(
              'Tea',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'ชา',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.water_drop,
              size: 25,
            ),
            onTap: () {
              print('Tea');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/5.jpg'),
            ),
            title: Text(
              'Waffle',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'วาฟเฟิล',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.heart_broken,
              size: 25,
            ),
            onTap: () {
              print('Waffle');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/6.jpg'),
            ),
            title: Text(
              'Donut',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'โดนัท',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.donut_small_outlined,
              size: 25,
            ),
            onTap: () {
              print('Donut');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/7.jpg'),
            ),
            title: Text(
              'Milk',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'นม',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.coffee_outlined,
              size: 25,
            ),
            onTap: () {
              print('Milk');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/8.jpg'),
            ),
            title: Text(
              'Croissant',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'ครัวซอง',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.bakery_dining_sharp,
              size: 25,
            ),
            onTap: () {
              print('Ice cream');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/9.jpg'),
            ),
            title: Text(
              'Coffee',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              'กาแฟ',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.coffee,
              size: 25,
            ),
            onTap: () {
              print('Coffee');
            },
          ),

          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/10.jpg'),
            ),
            title: Text(
              'Noodles',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'ก๋วยเตี๋ยว',
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(
              Icons.dinner_dining_outlined,
              size: 25,
            ),
            onTap: () {
              print('Noodles');
            },
          ),



        ],
      ),
    );
  }
}
